#En esta primera parte se toman todos los archivos fastq comprimidos y se descomprimen, toma la carpeta de la primera variable
#que se introduzca en la línea de comandos junto con bash pipeline
echo "Se analizarán las siguientes muestras"
for sid in $(ls input/$1 | cut -d "." -f1)
    do
    echo "$sid"
    echo "$sid" >> registroanálisis.txt
    done
wc -l registroanálisis.txt
#rm muestrastotales.txt
echo "Comenzando extracción"
for sid in $(ls input/$1 | cut -d "." -f1)
    do
    mkdir -p output/$1/datosextraidos
    gunzip -N -c input/$1/${sid}.fastq.gz > output/$1/datosextraidos/${sid}.fastq
    echo "Extracción de ${sid}.fastq completada"
    done
#Se comienza con la prueba de la calidad
echo "Comenzando Fastqc"
for sid in $(ls input/$1 | cut -d "." -f1)
    do
    mkdir -p output/$1/Quality/${sid}.fastq
    fastqc -T 3 --outdir output/$1/Quality/${sid}.fastq output/$1/datosextraidos/${sid}.fastq
    #firefox output/$1/Quality/${sid}.fastq/${sid}_fastqc.html Esta linea da problemas
    done
#La siguiente línea de código esta pensada para comprobar la calidad y ejecutar trimmomatic sobre las muestras antes de continuar con el mapeo.
echo "Pulsa enter para continuar con el mapeo "
read respuesta
    if [[ "$respuesta" == "" ]];
    then 
        echo "Continuando con mapeo"
        mkdir -p output/$1/Mapped/${sid}
        for sid in $(ls input/$1 | cut -d "." -f1)
        do
        /home/alba/software/STAR-2.7.6a/bin/Linux_x86_64/STAR --runMode alignReads --genomeLoad NoSharedMemory --clip3pAdapterSeq polyA --outSAMstrandField intronMotif --runThreadN 6 --genomeDir /home/alba/Desktop/TFM_Fernando_Gálvez/Análisis/Misanálisis/Otherdata/Mouse-g2cm39-r.104/Mice/STAR/g2cm39-r.104/GenomeDirMouse_GRCm39-104 --readFilesIn /home/alba/Desktop/TFM_Fernando_Gálvez/Análisis/Misanálisis/output/$1/datosextraidos/${sid}.fastq  --clip5pNbases 13 --sjdbGTFfile /home/alba/Desktop/TFM_Fernando_Gálvez/Análisis/Misanálisis/Otherdata/Mouse-g2cm39-r.104/Mus_musculus.GRCm39.104.gtf --outFileNamePrefix /home/alba/Desktop/TFM_Fernando_Gálvez/Análisis/Misanálisis/output/$1/Mapped/$sid/$sid --outSAMreadID Number --outSAMtype BAM SortedByCoordinate --outWigType wiggle --outFilterMultimapNmax 1 --outFilterMismatchNmax 10
        done    
    else  
        echo ""  
    fi
for sid in $(ls input/$1 | cut -d "." -f1)
    do
    mkdir -p output/$1/HTSEQ/${sid}
    htseq-count -f bam --order=pos --idattr=gene_id --stranded=no output/$1/Mapped/${sid}/${sid}Aligned.sortedByCoord.out.bam /home/alba/Desktop/TFM_Fernando_Gálvez/Análisis/Misanálisis/Otherdata/Mouse-g2cm39-r.104/Mus_musculus.GRCm39.104.gtf > output/$1/HTSEQ/${sid}/${sid}.txt 
    done
